#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define V 21
#define H 65
#define N 100

// V y H indica la longitud del campo/grilla.

typedef struct {
	int x, y;
	int modX, modY;
	char imagen;
} snk;

typedef struct {
	int x, y;
} frt;

// declaramos las variables de las structs.
snk snake[N]; // N representa lo "m�s grande" que puede llegar a ser la serpiente.
frt fruta;

// prototipos.
void inicio(int *tam, char campo[V][H]);
void intro_campo(char campo[V][H]);
void intro_datos(char campo[V][H], int tam);
void draw(char campo[V][H]);
void loop(char campo[V][H], int tam);
void input(char campo[V][H], int *tam, int *muerto);
void update(char campo[V][H], int tam);
void intro_datos2(char campo[V][H], int tam);

int main() {
	int tam; // tama�o de la serpiente en cada momento.
	char campo[V][H]; // matriz donde se recogeran todos los elementos.
	
	inicio(&tam, campo); // iniciar todos los elementos.
	loop(campo, tam);
	
	system("pause");
	return 0;
}

void inicio(int *tam, char campo[V][H]) {
	int i;
	
	// cabeza de la serpiente.
	snake[0].x = 32; // pos X.
	snake[0].y = 10; // pos Y.
	
	// tama�o de la serpiente.
	*tam = 4;
	
	// coordenadas de la fruta.
	srand(time(NULL)); // creamos la semilla.
	
	fruta.x = rand() % (H-1); // pos X de la fruta.
	fruta.y = rand() % (V-1); // pos Y de la fruta.
	
	// controlamos el caso donde X o Y sean cero.
	while (fruta.x == 0) {
		fruta.x = rand() % (H-1);
	}
	
	while (fruta.y == 0) {
		fruta.y = rand() % (V-1);
	}
	
	// colocamos valores a los modificadores de la struct snake.
	for (i=0; i < *tam; i++) {
		snake[i].modX = 1; // para que avance en horizontal.
		snake[i].modY = 0;
	}
	
	// creaci�n del campo de juego.
	intro_campo(campo);
	intro_datos(campo, *tam);
}

void intro_campo(char campo[V][H]) {
	int i, j;
	
	for (i=0; i < V; i++) {
		for (j=0; j < H; j++) {
			if (i == 0 || i == V-1) {
				campo[i][j] = '-';
			} else if (j == 0 || j == H-1) {
				campo[i][j] = '|';
			} else {
				campo[i][j] = ' ';
			}
		}
	}
}

void intro_datos(char campo[V][H], int tam) {
	int i;
	
	// creamos las coordenadas X, Y del resto del cuerpo de la serpiente y la imagen.
	for (i=1; i < tam; i++) {
		snake[i].x = snake[i-1].x - 1;
		snake[i].y = snake[i-1].y;
		
		snake[i].imagen = 'X';
	}
	
	snake[0].imagen = 'O';
	
	// metemos todas las coordenadas en la matriz campo.
	for (i=0; i < tam; i++) {
		campo[snake[i].y][snake[i].x] = snake[i].imagen;
	}
	
	// metemos las coordenadas de la fruta.
	campo[fruta.y][fruta.x] = '%';
}

void draw(char campo[V][H]) {
	int i, j;
	
	for (i=0; i < V; i++) {
		for (j=0; j < H; j++) {
			printf("%c", campo[i][j]);
		}
		printf("\n");
	}
}

void loop(char campo[V][H], int tam) {
	int muerto = 0; // 0: vivo | 1: muerto.
	
	do {
		system("cls"); // borra/limpia la pantalla.
		
		draw(campo);
		input(campo, &tam, &muerto);
		update(campo, tam);
		
	} while(muerto == 0);
}

void input(char campo[V][H], int *tam, int *muerto) {
	int i;
	char key;
	
	/*
	* escenarios de muerte:
	* 1) si la serpiente choca contra alg�n borde.
	* 2) si la serpiente se toca a s� misma.
	*/
	
	// primer check: contra los bordes.
	if (snake[0].x == 0 || snake[0].x == H-1 || snake[0].y == 0 || snake[0].y == V-1) {
		*muerto = 1;
	}
	
	// segundo check: contra el mismo cuerpo.
	for (i=1; i < *tam && *muerto == 0; i++) {
		if (snake[0].x == snake[i].x && snake[0].y == snake[i].y) {
			*muerto = 1;
		}
	}
	
	// tercer check: comprobar si comimos la fruta!
	if (snake[0].x == fruta.x && snake[0].y == fruta.y) {
		*tam += 1; // si comemos la fruta, incrementamos el tama�o de la serpiente en 1.
	
		// damos una nueva img a la serpiente.
		snake[*tam - 1].imagen = 'X';
		
		// regeneramos la fruta.
		fruta.x = rand() % (H-1);
		fruta.y = rand() % (V-1);
			
		while (fruta.x == 0) {
			fruta.x = rand() % (H-1);
		}
			
		while (fruta.y == 0) {
			fruta.y = rand() % (V-1);
		}
	}
	
	// comprobamos si pulsamos una tecla (mientras NO estemos muertos).
	if (*muerto == 0) {
		if (kbhit() == 1) {
			key = getch();
			
			// mov hacia abajo.
			if (key == '2' && snake[0].modY != -1) {
				snake[0].modX = 0;
				snake[0].modY = 1;
			}
			
			// mov hacia arriba.
			if (key == '8' && snake[0].modY != 1) {
				snake[0].modX = 0;
				snake[0].modY = -1;
			}
			
			// mov hacia la izq.
			if (key == '4' && snake[0].modX != 1) { 
				snake[0].modX = -1;
				snake[0].modY = 0;
			}
			
			// mov hacia la derecha.
			if (key == '6' && snake[0].modX != -1) {
				snake[0].modX = 1;
				snake[0].modY = 0;
			}
		
		}
	}
}

void update(char campo[V][H], int tam) {
	// borramos todos los datos de la matriz.
	intro_campo(campo);
	
	// introducimos los nuevos datos.
	intro_datos2(campo, tam);
}

void intro_datos2(char campo[V][H], int tam) {
	int i;
	
	// creamos el "seguimiento" del cuerpo a la cabeza de la serpiente.
	for (i=tam-1; i>0; i--) {
		snake[i].x = snake[i-1].x;
		snake[i].y = snake[i-1].y;
	}
	
	// actualizamos la cabeza.
	snake[0].x += snake[0].modX;
	snake[0].y += snake[0].modY;
	
	// introducimos los cambios en la matriz.
	for (i=0; i < tam; i++) {
		campo[snake[i].y][snake[i].x] = snake[i].imagen;
	}
	
	// introducimos los cambios respecto a la fruta.
	campo[fruta.y][fruta.x] = '%';
}
