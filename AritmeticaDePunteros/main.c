#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n = 5;
    int *pi = &n; // cada posicion apuntada por pi (entero) ocupa 4 bytes.

    char c = 'A';
    char *pc = &c; // cada posicion apuntada por pc (caracter) ocupa 1 byte.

    printf("Antes pi: %p y pc: %p\n", pi, pc);
    pi++; // apunta al siguiente entero.
    pc++; // apunta al siguiente caracter.
    printf("Despues pi: %p y pc: %p\n", pi, pc);

    return 0;
}
