#include <stdio.h>
#include <stdlib.h>

int main()
{
    int listaEnteros[5];
    int i;

    for (i=0; i < 5; i++) {
        listaEnteros[i] = i*2;
    }

//    for (i=0; i < 5; i++) {
//        printf("listaEnteros[%d] = %d\n", i, listaEnteros[i]);
//    }

    // ejemplo de error.
    for (i=0; i < 6; i++) {
        printf("listaEnteros[%d] = %d\n", i, listaEnteros[i]);
    }

    return 0;
}
