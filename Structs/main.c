#include <stdio.h>
#include <stdlib.h>

struct PERSON {
    char name[100];
    int age;
};

void llenarDatos(struct PERSON *person, const char *name, int age) {
    strcpy(person->name, name);
    person->age = age;
}

int main()
{
    struct PERSON person;

    char nombre[100];
    int edad;

    printf("Ingresa el nombre: \n");
    gets(nombre);

    printf("Ingresa la edad: \n");
    scanf(" %i", &edad);

    llenarDatos(&person, nombre, edad);
    printf("Name = %s, age = %d\n", person.name, person.age);

    return 0;
}
