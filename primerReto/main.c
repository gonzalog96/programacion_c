#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *cadenaMasLarga(char *cadena1, char *cadena2, char *cadena3) {
    if (strlen(cadena1) > strlen(cadena2) && strlen(cadena1) > strlen(cadena3)) {
        return cadena1;
    } else if (strlen(cadena2) > strlen(cadena1) && strlen(cadena2) > strlen(cadena3)) {
        return cadena2;
    } else if (strlen(cadena3) > strlen(cadena2)) {
        return cadena3;
    }
    return "son iguales";
}

int main()
{
    char cadena1[20];
    char cadena2[20];
    char cadena3[20];

    printf("Ingresa la primer cadena: \n");
    gets(cadena1);

    printf("Ingresa la segunda cadena: \n");
    gets(cadena2);

    printf("Ingresa la tercer cadena: \n");
    gets(cadena3);

    printf("La cadena mas larga es: %s", cadenaMasLarga(cadena1, cadena2, cadena3));

    return 0;
}
