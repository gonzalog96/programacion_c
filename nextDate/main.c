#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	time_t oneDay = 24 * 60 * 60;
	time_t today = time(NULL);
	time_t nextDate;
	
	int days;
	printf("Ingresa la cant. de dias: \n");
	scanf("%i", &days);
	
	nextDate = today + oneDay * days;
	
	printf("%s\n", ctime(&nextDate));
	
	system("pause");
	return 0;
}
