#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
//    for (int i=0; i < argc; i++) {
//        printf("Argumento %d = %s", i, argv[i]);
//    }

    // buffer overflow.
    char buffer[20];
    char *buffer2 = "esto sera sobreescrito";

    printf("Original buffer2: %s\n", buffer2);
    strcpy(buffer, "estoOcupaMasDeVeinteCaracteres");
    printf("New buffer2: %s\n", buffer2);

    return 0;
}
