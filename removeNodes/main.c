#include <stdio.h>

typedef struct Node {
	int number;
	struct Node *next;
} NODE;

NODE *start = NULL, *current, *next;

NODE *createNode(int number)
{
	NODE *newNode;

	newNode = malloc(sizeof(NODE));
	newNode->next = NULL;
	newNode->number = number;

	return newNode;
}

void removeNodeFromList(int number) {
	int goOn = 1;
	
	current = start;
	while (current && goOn) {
		if (current->number == number) {
			start = current->next;
			goOn = 0;
		} else {
			if (current->next->number == number) {
				current->next = current->next->next;
				goOn = 0;
			}
		}
		current = current->next;
	}
}

void cargarNodos() {
	char goOn;
	int listSize = 0, number;
	
	do {
		printf( "La lista contiene %d nodos. Ingrese el siguiente numero (0 para finalizar)\n", listSize );
		scanf("%d", &number );
		if ( number ) {
			if ( !start ) {
				start = createNode( number );
				listSize++;
			} else {
				current = start;
				while ( current->next ) {
					current = current->next;
				}
				current->next = createNode( number );
				listSize++;
			}
			goOn = 1;
		} else {
			goOn = 0;
		}
	} while ( goOn );
}

void imprimirNodos() {
	current = start;
	
	printf( "La lista contiene los numeros: \n" );
	while (current) {
		printf( "%d", current->number );
		printf( current->next ? ", " : "\n" );
		current = current->next;
	}
}

void liberarMemoria() {
	current = start;
	while (current) {
		next = current->next;
		free( current );
		current = next;
	}
}

int main() {
	cargarNodos();
	
	imprimirNodos();
	removeNodeFromList(2);
	imprimirNodos();
	
	// OPCIONAL: para liberar memoria.
	liberarMemoria();
	
	system("pause");
	return 0;
}
