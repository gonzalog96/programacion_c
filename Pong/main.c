#include <stdio.h>
#include <windows.h>

#define V 21
#define H 75

void inicio(char campo[V][H], int pelX, int pelY, int iniJug, int finJug, int iniIA, int finIA);
void borde(char campo[V][H]);
void raquetaJug(char campo[V][H], int iniJug, int finJug);
void raquetaIA(char campo[V][H], int iniIA, int finIA);
void pelota(char campo[V][H], int pelX, int pelY);
void leerCampo(char campo[V][H]);
void gameLoop(char campo[V][H], int pelX, int pelY, int iniJug, int finJug, int iniIA, int finIA, int modX, int modY, int modIA);
void draw(char campo[V][H]);
void input(char campo[V][H], int *pelX, int *pelY, int *iniJug, int *finJug, int *iniIA, int *finIA, int *modX, int *modY, int *modIA, int *gol);
void update(char campo[V][H], int pelX, int pelY, int iniJug, int finJug, int iniIA, int finIA);


int main() {
	int pelX, pelY, iniJug, finJug, iniIA, finIA; // var de posicion.
	int modX, modY, modIA; // modificacion.
	
	char campo[V][H];
	
	// posicion.
	pelX = 37;
	pelY = 10;
	
	iniJug = 8;
	finJug = 12;
	
	iniIA = 5;
	finIA = 18;
	
	// modificacion.
	modX = -1;
	modY = -1;
	modIA = -1;
	
	inicio(campo, pelX, pelY, iniJug, finJug, iniIA, finIA);
	gameLoop(campo, pelX, pelY, iniJug, finJug, iniIA, finIA, modX, modY, modIA); // bucle del juego.
	
	system("pause");
	return 0;
}

void inicio(char campo[V][H], int pelX, int pelY, int iniJug, int finJug, int iniIA, int finIA) {
	borde(campo);	
	raquetaJug(campo, iniJug, finJug);
	raquetaIA(campo, iniIA, finIA);
	pelota(campo, pelX, pelY);
}

void borde(char campo[V][H]) {
	int i, j;
	
	for (i=0; i < V; i++) {
		for (j=0; j < H; j++) {
			if (i==0 || i==V-1) {
				campo[i][j] = '-';
			} else if (j == 0 || j == H-1) {
				campo[i][j] = '|';
			} else {
				campo[i][j] = ' ';
			}
		}
	}
}

void raquetaJug(char campo[V][H], int iniJug, int finJug) {
	int i, j;
	
	for (i = iniJug; i <= finJug; i++) {
		for (j=2; j <= 3; j++) {
			campo[i][j] = 'X';
		}
	}
}

void raquetaIA(char campo[V][H], int iniIA, int finIA) {
	int i, j;
	
	for (i=iniIA; i <= finIA; i++) {
		for (j=H-4; j <= H-3; j++) {
			campo[i][j] = 'X';
		}
	}
}

void pelota(char campo[V][H], int pelX, int pelY) {
	campo[pelY][pelX] = 'O';
}

void leerCampo(char campo[V][H]) {
	int i, j;
	
	for (i=0; i < V; i++) {
		for (j=0; j < H; j++) {
			printf("%c", campo[i][j]);
		}
		printf("\n");
	}
}

void gameLoop(char campo[V][H], int pelX, int pelY, int iniJug, int finJug, int iniIA, int finIA, int modX, int modY, int modIA){
	int gol = 0;
	
	do {
		draw(campo); // dibujar en pantalla.
		input(campo, &pelX, &pelY, &iniJug, &finJug, &iniIA, &finIA, &modX, &modY, &modIA, &gol); // verificar y modificar las posiciones.
		update(campo, pelX, pelY, iniJug, finJug, iniIA, finIA); // actualizar la matriz campo.
		
		Sleep(10);
	} while (gol == 0);
}

void draw(char campo[V][H]) {
	system("cls");
	leerCampo(campo);
}

void input(char campo[V][H], int *pelX, int *pelY, int *iniJug, int *finJug, int *iniIA, int *finIA, int *modX, int *modY, int *modIA, int *gol) {
	int i;
	char key;
	
	// grupo de verificacion.
	if ((*pelY) == 1 || (*pelY) == V - 2) {
		*modY *= -1;
	}
	
	if ((*pelX) == 1 || (*pelX) == H - 2) {
		*gol = 1;
	}
	
	if ((*pelX) == 4) {
		for (i = (*iniJug); i <= (*finJug); i++) {
			if (i == (*pelY)) {
				*modX *= -1;
			}
		}
	}
	
	if (*pelX == H - 5) {
		for (i = (*iniIA); i <= (*finIA); i++) {
			if (i == (*pelY)) {
				*modX *= -1;
			}
		}
	}
	
	if (*iniIA == 1 || *finIA == V-1) {
		*modIA *= -1;
	}
	
	// grupo de modificacion.
	
	if (*gol == 0) {
		// pelota.
		*pelX += (*modX);
		*pelY += (*modY);
		
		// raqueta de la IA.
		*iniIA += (*modIA);
		*finIA += (*modIA);	
		
		// raquete del jugador.
		if (kbhit() == 1) {
			key = getch();
			
			if (key == '8') {
				if (*iniJug != 1) {
					*iniJug -= 1;
					*finJug -= 1;
				}
			}
			
			if (key == '2') {
				if (*finJug != V-2) {
					*iniJug += 1;
					*finJug += 1;
				}
			}
		}
	}
}

void update(char campo[V][H], int pelX, int pelY, int iniJug, int finJug, int iniIA, int finIA) {
	borde(campo);	
	raquetaJug(campo, iniJug, finJug);
	raquetaIA(campo, iniIA, finIA);
	pelota(campo, pelX, pelY);
}
