#include <stdio.h>
#include <stdlib.h>

#include "operaciones.h"

float solicitarValor() {
    float valor;

    printf("Ingresa un valor: \n");
    scanf("%f", &valor);

    return valor;
}

void obtenerResultado(int tipoOperacion) {
    float primerValor, segundoValor, resultado;
    primerValor = solicitarValor();
    segundoValor = solicitarValor();

    switch(tipoOperacion) {
        case 1: // suma.
            resultado = suma(primerValor, segundoValor);
            printf("El resultado de la suma entre %f y %f es: %f\n", primerValor, segundoValor, resultado);
            break;
        case 2: // resta.
            resultado = resta(primerValor, segundoValor);
            printf("El resultado de la diferencia entre %f y %f es: %f\n", primerValor, segundoValor, resultado);
            break;
        case 3: // producto.
            resultado = producto(primerValor, segundoValor);
            printf("El resultado del producto entre %f y %f es: %f\n", primerValor, segundoValor, resultado);
            break;
        case 4: // division.
            resultado = division(primerValor, segundoValor);
            printf("El resultado de la division entre %f y %f es: %f\n", primerValor, segundoValor, resultado);
            break;
    }
}

int main()
{
    int opcion;

    printf(":: Calculadora de operaciones basicas matematicas :: \n");
    printf("Por favor, ingresa alguna de las siguientes opciones: \n");
    printf("1. Suma entre dos valores.\n");
    printf("2. Diferencia entre dos valores.\n");
    printf("3. Producto entre dos valores.\n");
    printf("4. Division entre dos valores.\n");
    printf("---\n");

    scanf("%i", &opcion);

    // convencion:
    // operacion = 1 -> suma; operacion = 2 -> resta;
    // operacion = 3 -> producto; operacion = 4 -> division.
    switch(opcion) {
        case 1:
            obtenerResultado(1);
            break;
        case 2:
            obtenerResultado(2);
            break;
        case 3:
            obtenerResultado(3);
            break;
        case 4:
            obtenerResultado(4);
            break;
        default:
            printf("Opcion invalida!");
            break;
    }
    return 0;
}
