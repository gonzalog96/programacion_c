float suma(float a, float b) {
    return a+b;
}

float resta(float a, float b) {
    return a-b;
}

float producto(float a, float b) {
    return a*b;
}

float division(float a, float b) {
    if (b != 0)
        return a/b;
    return -1; // no es posible ejecutar la division por cero.
}
