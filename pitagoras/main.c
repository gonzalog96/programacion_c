#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main() {
	int a, b;
	
	printf("Ingresa el cateto 1: \n");
	scanf("%i", &a);
	
	printf("Ingresa el cateto 2: \n");
	scanf("%i", &b);
	
	printf("La hipotenusa es: %.2f\n", sqrt(pow(a, 2) + pow(b, 2)));
	
	
	system("pause");
	return 0;
}
