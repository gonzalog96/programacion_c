#include <stdio.h>
#include <stdlib.h>

int main()
{
    char *name = "Gonza";
    printf("Nombre: %s (%p)\n", name, name);

//    for (int i=0; i < 5; i++) {
//        printf("name[%d](%p) = %c\n", i, name+i, *(name+i));
//    }

    // otra forma (dualidad puntero-indice)
    for (int i=0; i < 5; i++) {
        printf("name[%d](%p) = %c\n", i, name+i, name[i]);
    }

    return 0;
}
